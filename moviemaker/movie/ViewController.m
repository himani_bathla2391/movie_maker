//
//  ViewController.m
//  movie
//
//  Created by Clicklabs 104 on 10/9/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UIButton *play;
@property (weak, nonatomic) IBOutlet UIImageView *bg;

@property (weak, nonatomic) IBOutlet UIButton *choose;


@end

@implementation ViewController
@synthesize image;
@synthesize bg;
@synthesize play;
@synthesize choose;
UIImage *img1;
UIImage *img2;
UIImage *img3;
UIImage *img4;
UIImage *img5;
UIImage *img6;
UIImage *img7;
UIImage *img8;
UIImage *img9;
UIImage *img10;
UIImage *img11;
UIImage *img12;
UIImage *img13;
UIImage *img14;
UIImage *img15;
UIImage *img16;
UIImage *img17;
UIImage *img;
AVAudioPlayer *audioPlayer;
//MPMoviePlayerViewController *moviePlayer;
- (void)viewDidLoad {
    [super viewDidLoad];
    img1= [UIImage imageNamed:@"1.jpg"];
    img2= [UIImage imageNamed:@"2.jpg"];
    img3= [UIImage imageNamed:@"3.jpg"];
    img4= [UIImage imageNamed:@"4.jpg"];
    img5= [UIImage imageNamed:@"5.jpg"];
    img6= [UIImage imageNamed:@"6.jpg"];
    img7= [UIImage imageNamed:@"7.jpg"];
    img8= [UIImage imageNamed:@"8.jpg"];
    img9= [UIImage imageNamed:@"9.jpg"];
    img10= [UIImage imageNamed:@"10.jpg"];
    img11= [UIImage imageNamed:@"11.jpg"];
    img12= [UIImage imageNamed:@"12.jpg"];
    img13= [UIImage imageNamed:@"13.jpg"];
    img14= [UIImage imageNamed:@"14.jpg"];
    img15= [UIImage imageNamed:@"15.jpg"];
    img16= [UIImage imageNamed:@"16.jpg"];
    img17= [UIImage imageNamed:@"17.jpg"];
    img= [UIImage imageNamed:@"imgres-6.jpg"];
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)choose:(id)sender {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    
    //imagePickerController.mediaTypes = [NSArray arrayWithObject:(NSString *)image];
    
//    popoverController = [[UIPopoverController alloc] initWithContentViewController:imagePickerController];
//    [popoverController presentPopoverFromRect:CGRectMake(0.0, 0.0, 400.0, 300.0)
//                                       inView:self.view
//                     permittedArrowDirections:UIPopoverArrowDirectionAny
//                                     animated:YES];
    
    [self presentModalViewController:imagePickerController animated:YES];
    
}


- (IBAction)play:(id)sender {
    
    image.animationImages = [NSArray arrayWithObjects: img1,img2,img3,img4,img5,img6,img7,img8,img9,img10,img11,img12,img13,img14,img15,img16,img17,nil];
    image.animationDuration = 13.0;
    image.animationRepeatCount=1;
    [image startAnimating];
    NSString *path = [[NSBundle mainBundle]
                      pathForResource:@"agnee" ofType:@"mp3"];
    audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:
                   [NSURL fileURLWithPath:path] error:NULL];
    [audioPlayer play];
//    image.image = image.animationImages.lastObject;
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)img editingInfo:(NSDictionary *)editInfo {
    [[picker parentViewController]dismissModalViewControllerAnimated:YES];
   bg.image=img;
// image.animationImages= img;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
